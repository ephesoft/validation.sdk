image: ephesoft/node.aws:4

definitions:
  services:
    docker-sonarcloud:
      memory: 3072  # Increase memory for Sonarcloud scan pipe
      type: docker
  caches:
    sonar: .sonar/cache  # Caching SonarCloud artifacts will speed up your build https://jira.atlassian.com/browse/BCLOUD-18478
  steps:
    - step: &audit-check
        name: NPM Audit
        script:
          - npm audit --registry=https://registry.npmjs.org --audit-level=moderate --omit=dev

    - step: &tag
        name: Tag Version
        script:
          - export PACKAGE_VERSION=`node -p "require('./package.json').version"`
          - git tag -a v$PACKAGE_VERSION -m $PACKAGE_VERSION # Create tag from package version
        after-script:
          - git push origin --tags # Push tags back to git server.  Never fail (allows re-runs)

    - step: &build-lint
        name: Build and Lint
        script:
          - npm ci
          - npm run build
          - npm run lint:report
        artifacts:
          - lint/**

    - step: &test-unit
        name: Run Unit Tests
        script:
          - npm ci
          - npm run test:unit
        artifacts:
          - .nyc_output/**
          - .sonarcloud/**

    - step: &publish
       name: Publish Package to NPM
       script:
          - npm ci
          - npm run build
          - npm run validate:ga
          - export NPM_REGISTRY_URL=registry.npmjs.org/ # Note: The trailing slash is needed to be properly resolved in the .npmrc file
          - export MYGET_REPO_PASSWORD=$NPM_PUBLIC_ACCESS_TOKEN # Update token to use token for publishing package to NPM (not our MYGET instance)
          - npm publish ./dist --access public

    - step: &sonarcloud-scan
        name: SonarCloud Scan
        clone:
          depth: full
        caches:
          - sonar
        services: [docker-sonarcloud]
        script:
          - npm install -g nyc # Install nyc for coverage report
          - npm run test:coverage
          - chmod 777 .sonar -R || true # required to create sonar cache
          - pipe: sonarsource/sonarcloud-scan:1.4.0

    - step: &sonarcloud-gate
        name: SonarCloud Gate
        caches:
          - sonar
        services: [docker-sonarcloud]
        script:
          - npm install -g nyc # Install nyc for coverage report
          - npm run test:coverage
          - chmod 777 .sonar -R || true # required to create sonar cache
          - pipe: sonarsource/sonarcloud-scan:1.4.0
          - pipe: sonarsource/sonarcloud-quality-gate:0.1.6

pipelines:
  custom:
    SonarCloud:
      - parallel:
        - step: *build-lint
        - step: *test-unit
      - step: *sonarcloud-scan
    Beta:
      - step:
          name: Publish Beta Package to NPM
          script:
              - npm ci
              - npm run create:beta
              - npm run validate:beta
              - npm run build
              - export NPM_REGISTRY_URL=registry.npmjs.org/ # Note: The trailing slash is needed to be properly resolved in the .npmrc file
              - export MYGET_REPO_PASSWORD=$NPM_PUBLIC_ACCESS_TOKEN # Update token to use token for publishing package to NPM (not our MYGET instance)
              - npm publish ./dist --access public --tag beta
  branches:
    master:
    - parallel:
      - step: *build-lint
      - step: *test-unit
    - parallel:
      - step: *tag
      - step: *sonarcloud-scan
      - step: *publish

  pull-requests:
    '**':
    - parallel:
      - step: *audit-check
      - step: *build-lint
      - step: *test-unit
    - step: *sonarcloud-gate
