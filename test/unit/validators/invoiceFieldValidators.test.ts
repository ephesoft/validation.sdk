import { expect } from 'chai';

import { InvoiceFieldValidators } from '../../../src';

describe('Test InvoiceValidators.isValidIsoDate', (): void => {
    it('should return true for valid ISO 8601 Dates', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('2020-03-31')).to.be.true;
        expect(InvoiceFieldValidators.isValidIsoDate('2020-02-29')).to.be.true;
    });

    it('should return false for valid date formats but invalid date in leap year', (): void => {
        /** leap year */
        expect(InvoiceFieldValidators.isValidIsoDate('2020-02-30')).to.be.false;
    });

    it('should return false for invalid date formats not like (yyyy-mm-dd)', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('20-02-2020')).to.be.false;
        expect(InvoiceFieldValidators.isValidIsoDate('2019-3-30')).to.be.false;
        expect(InvoiceFieldValidators.isValidIsoDate('19-03-29')).to.be.false;
        expect(InvoiceFieldValidators.isValidIsoDate('2020-03-3')).to.be.false;
    });

    it('should return false for valid date formats but invalid dates', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('2020-03-32')).to.be.false;
        expect(InvoiceFieldValidators.isValidIsoDate('2019-13-20')).to.be.false;
    });

    it('should return true with flags ignore separators enabled', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('2020/04/03', { ignoreSeparators: true })).to.be.true;
        expect(InvoiceFieldValidators.isValidIsoDate('2020  04  03', { ignoreSeparators: true })).to.be.true;
        expect(InvoiceFieldValidators.isValidIsoDate('04   03   2000', { ignoreSeparators: true })).to.be.false;
    });

    it('should return false with flags ignore separators enabled', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('04   03   2000', { ignoreSeparators: true })).to.be.false;
        expect(InvoiceFieldValidators.isValidIsoDate('04   2000   03', { ignoreSeparators: true })).to.be.false;
    });

    it('should return true with flag separator SLASH', (): void => {
        expect(InvoiceFieldValidators.isValidIsoDate('2020/04/03', { ignoreSeparators: false, separator: '/' })).to.be.true;
    });
});

describe('Test InvoiceValidators.isValidNumber', (): void => {
    it('should return true for valid numbers', (): void => {
        expect(InvoiceFieldValidators.isValidNumber('-2000.000001')).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber('2000.000001')).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber('-2000')).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber('2000')).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber(2000)).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber(-2000)).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber(2000.9999999)).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber(2000.01)).to.be.true;
        /** The trailing dot and + are stripped off and result in valid numbers */
        expect(InvoiceFieldValidators.isValidNumber(2000)).to.be.true;
        expect(InvoiceFieldValidators.isValidNumber(+2000)).to.be.true;
    });

    it('should return false for invalid numbers', (): void => {
        expect(InvoiceFieldValidators.isValidNumber('+2000')).to.be.false;
        expect(InvoiceFieldValidators.isValidNumber('2000.')).to.be.false;
        expect(InvoiceFieldValidators.isValidNumber('2000..00')).to.be.false;
        expect(InvoiceFieldValidators.isValidNumber('-2000.')).to.be.false;
        expect(InvoiceFieldValidators.isValidNumber('-2000..00')).to.be.false;
    });
});
