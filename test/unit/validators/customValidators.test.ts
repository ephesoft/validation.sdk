import { expect } from 'chai';

import { customWordsRegex } from '../../../src';

describe('Custom Regex', (): void => {
    //*** Positive Test cases ***/
    it('should return true if the role matches the custom regex', (): void => {
        expect(customWordsRegex(['admin', 'user']).test('admin')).to.equal(true);
        expect(customWordsRegex(['admin', 'user']).test('user')).to.equal(true);
    });

    //*** Negative Test cases ***/
    it('should return false if the role doesn`t match the custom regex', (): void => {
        expect(customWordsRegex(['admin', 'user']).test('randomName')).to.equal(false);
    });
});
