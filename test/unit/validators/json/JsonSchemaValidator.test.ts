import { expect } from 'chai';
import { executeAndExpectError } from '@ephesoft/test.assertions';
import { JsonSchemaValidator } from '../../../../src/validators/json/JsonSchemaValidator';
import { InvalidJsonSchemaError } from '../../../../src/validators/json/InvalidJsonSchemaError';
import { UnknownPropertiesBehavior } from '../../../../src/validators/json/UnknownPropertiesBehavior';
import { InvalidParameterError } from '@ephesoft/core.sdk/errors/InvalidParameterError';

describe('JsonValidator unit tests', function () {
    describe('constructor', function () {
        it('should throw error if schema is invalid', function () {
            executeAndExpectError(
                () => {
                    new JsonSchemaValidator({ this: 'is invalid' });
                },
                InvalidJsonSchemaError,
                'Schema is invalid: strict mode: unknown keyword: "this"'
            );
        });

        it('should throw an error if there is a circular reference in schema', function () {
            const schema = {
                type: 'object',
                properties: {}
            };
            (schema.properties as Record<string, object>).inner = schema;

            const inner: Record<string, object> = {};
            const circularObjectSchema = { inner };
            inner.outer = circularObjectSchema;

            // Schema is processed immediately - if we have an unhandled cirular reference we would get a stack overflow
            executeAndExpectError(
                () => {
                    new JsonSchemaValidator(circularObjectSchema);
                },
                InvalidJsonSchemaError,
                'Schema is invalid: Maximum call stack size exceeded'
            );
        });

        it('should throw error if unknownPropertiesBehavior is invalid', function () {
            executeAndExpectError(
                () => {
                    // @ts-expect-error - Force in bad value
                    new JsonSchemaValidator({ type: 'object' }, { unknownPropertiesBehavior: 'test' });
                },
                InvalidParameterError,
                'Unsupported unknown properties behavior: "test"'
            );
        });
    });

    it('should allow union types', function () {
        const validator = new JsonSchemaValidator({ type: 'object', properties: { key: { type: ['string', 'number'] } } });
        expect(validator.validate({ key: 42 })).to.deep.equal([]);
    });

    it('should allow union type with only one value', function () {
        const validator = new JsonSchemaValidator({ type: ['object'], properties: { key: { type: ['number'] } } });
        expect(validator.validate({ key: 42 })).to.deep.equal([]);
    });

    it('should allow discriminators', function () {
        const validator = new JsonSchemaValidator({
            type: 'object',
            discriminator: { propertyName: 'type' },
            oneOf: [
                {
                    properties: {
                        type: { const: 'string' },
                        value: { type: 'string' }
                    },
                    required: ['type', 'value']
                },
                {
                    properties: {
                        type: { const: 'number' },
                        value: { type: 'number' }
                    },
                    required: ['type', 'value']
                }
            ]
        });
        expect(validator.validate({ type: 'string', value: 'text value' })).to.deep.equal([]);
        expect(validator.validate({ type: 'number', value: 42 })).to.deep.equal([]);
        expect(validator.validate({ type: 'number', value: 'text value' })).to.deep.equal([
            {
                code: 'type',
                message: 'must be number',
                location: {
                    objectPath: '/value',
                    schemaPath: '#/oneOf/1/properties/value/type'
                },
                context: {
                    type: 'number'
                }
            }
        ]);
    });

    it('should not coerce types', function () {
        const validator = new JsonSchemaValidator({ type: 'number' });
        expect(validator.validate('42')).to.deep.equal([
            {
                code: 'type',
                message: 'must be number',
                location: {
                    objectPath: '',
                    schemaPath: '#/type'
                },
                context: {
                    type: 'number'
                }
            }
        ]);
    });

    it('should perform strict number verification', function () {
        const validator = new JsonSchemaValidator({
            type: 'object',
            properties: {
                key1: { type: 'number' },
                key2: { type: 'number' },
                key3: { type: 'number' },
                key4: { type: 'number' }
            },
            required: ['key1', 'key2', 'key3', 'key4']
        });
        expect(
            validator.validate({
                key1: NaN,
                key2: Infinity,
                key3: 42,
                key4: 42.42
            })
        ).to.deep.equal([
            {
                code: 'type',
                message: 'must be number',
                location: {
                    objectPath: '/key1',
                    schemaPath: '#/properties/key1/type'
                },
                context: {
                    type: 'number'
                }
            },
            {
                code: 'type',
                message: 'must be number',
                location: {
                    objectPath: '/key2',
                    schemaPath: '#/properties/key2/type'
                },
                context: {
                    type: 'number'
                }
            }
        ]);
    });

    it('should return empty array if object is valid according to the schema', function () {
        const validator = new JsonSchemaValidator({ type: 'object' });
        expect(validator.validate({})).to.deep.equal([]);
    });

    it('should fail if required property is missing', function () {
        const validator = new JsonSchemaValidator({
            type: 'object',
            properties: {
                key: { type: 'string' }
            },
            required: ['key']
        });
        expect(validator.validate({})).to.deep.equal([
            {
                code: 'required',
                message: "must have required property 'key'",
                location: {
                    objectPath: '',
                    schemaPath: '#/required'
                },
                context: {
                    missingProperty: 'key'
                }
            }
        ]);
    });

    it('should fail if property is wrong type', function () {
        const validator = new JsonSchemaValidator({
            type: 'object',
            properties: {
                key: { type: 'string' }
            },
            required: ['key']
        });
        expect(validator.validate({ key: 42 })).to.deep.equal([
            {
                code: 'type',
                message: 'must be string',
                location: {
                    objectPath: '/key',
                    schemaPath: '#/properties/key/type'
                },
                context: {
                    type: 'string'
                }
            }
        ]);
    });

    it('should use UnknownPropertiesBehavior.AllowUnlessExplicitlyProhibited by default', function () {
        const validator = new JsonSchemaValidator({
            type: 'object',
            properties: {
                level2: {
                    type: 'object',
                    properties: {
                        noise1: {
                            type: 'number'
                        },
                        noise2: {
                            type: 'string'
                        },
                        array1: {
                            type: 'array',
                            items: {
                                type: 'object'
                            }
                        },
                        level3: {
                            type: 'object'
                        },
                        specified: {
                            type: 'object',
                            additionalProperties: false
                        }
                    }
                }
            }
        });
        expect(
            validator.validate({
                level2: {
                    level3: {
                        unknown3: 'should be OK'
                    },
                    array1: [
                        {},
                        {
                            unknownItemProperty: 'should be OK'
                        }
                    ],
                    unknown2: 'should be OK',
                    specified: {
                        unknown4: 'disallowed by schema; allowed by override - schema should win'
                    }
                },
                unknown1: 'should be OK'
            })
        ).to.deep.equal([
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/specified',
                    schemaPath: '#/properties/level2/properties/specified/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown4'
                }
            }
        ]);
    });

    it('should respect UnknownPropertiesBehavior.ProhibitUnlessExplicitlyAllowed', function () {
        const validator = new JsonSchemaValidator(
            {
                type: 'object',
                properties: {
                    level2: {
                        type: 'object',
                        properties: {
                            noise1: {
                                type: 'number'
                            },
                            noise2: {
                                type: 'string'
                            },
                            array1: {
                                type: 'array',
                                items: {
                                    type: 'object'
                                }
                            },
                            level3: {
                                type: 'object'
                            },
                            specified: {
                                type: 'object',
                                additionalProperties: true
                            }
                        }
                    }
                }
            },
            {
                unknownPropertiesBehavior: UnknownPropertiesBehavior.ProhibitUnlessExplicitlyAllowed
            }
        );
        expect(
            validator.validate({
                level2: {
                    level3: {
                        unknown3: 'should not be here'
                    },
                    array1: [
                        {},
                        {
                            unknownItemProperty: 'should not be here'
                        }
                    ],
                    unknown2: 'should not be here',
                    specified: {
                        unknown4: 'allowed by schema; disallowed by override - schema should win'
                    }
                },
                unknown1: 'should not be here'
            })
        ).to.deep.equal([
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '',
                    schemaPath: '#/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown1'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2',
                    schemaPath: '#/properties/level2/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown2'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/array1/1',
                    schemaPath: '#/properties/level2/properties/array1/items/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknownItemProperty'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/level3',
                    schemaPath: '#/properties/level2/properties/level3/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown3'
                }
            }
        ]);
    });

    it('should respect UnknownPropertiesBehavior.ProhibitAll', function () {
        const validator = new JsonSchemaValidator(
            {
                type: 'object',
                properties: {
                    level2: {
                        type: 'object',
                        properties: {
                            noise1: {
                                type: 'number'
                            },
                            noise2: {
                                type: 'string'
                            },
                            array1: {
                                type: 'array',
                                items: {
                                    type: 'object'
                                }
                            },
                            level3: {
                                type: 'object'
                            },
                            specified: {
                                type: 'object',
                                additionalProperties: true
                            }
                        }
                    }
                }
            },
            { unknownPropertiesBehavior: UnknownPropertiesBehavior.ProhibitAll }
        );
        expect(
            validator.validate({
                level2: {
                    level3: {
                        unknown3: 'should not be here'
                    },
                    array1: [
                        {},
                        {
                            unknownItemProperty: 'should not be here'
                        }
                    ],
                    unknown2: 'should not be here',
                    specified: {
                        unknown4: 'allowed by schema; prohibited by override - override should win'
                    }
                },
                unknown1: 'should not be here'
            })
        ).to.deep.equal([
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '',
                    schemaPath: '#/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown1'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2',
                    schemaPath: '#/properties/level2/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown2'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/array1/1',
                    schemaPath: '#/properties/level2/properties/array1/items/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknownItemProperty'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/level3',
                    schemaPath: '#/properties/level2/properties/level3/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown3'
                }
            },
            {
                code: 'additionalProperties',
                message: 'must NOT have additional properties',
                location: {
                    objectPath: '/level2/specified',
                    schemaPath: '#/properties/level2/properties/specified/additionalProperties'
                },
                context: {
                    additionalProperty: 'unknown4'
                }
            }
        ]);
    });

    it('should respect configuration.unknownProperties = UnknownPropertiesBehavior.AllowAll', function () {
        const validator = new JsonSchemaValidator(
            {
                type: 'object',
                properties: {
                    level2: {
                        type: 'object',
                        properties: {
                            noise1: {
                                type: 'number'
                            },
                            noise2: {
                                type: 'string'
                            },
                            array1: {
                                type: 'array',
                                items: {
                                    type: 'object'
                                }
                            },
                            level3: {
                                type: 'object'
                            },
                            specified: {
                                type: 'object',
                                additionalProperties: false
                            }
                        }
                    }
                }
            },
            { unknownPropertiesBehavior: UnknownPropertiesBehavior.AllowAll }
        );
        expect(
            validator.validate({
                level2: {
                    level3: {
                        unknown3: 'should be OK'
                    },
                    array1: [
                        {},
                        {
                            unknownItemProperty: 'should be OK'
                        }
                    ],
                    unknown2: 'should be OK',
                    specified: {
                        unknown4: 'disallowed by schema; allowed by override - override should win'
                    }
                },
                unknown1: 'should be OK'
            })
        ).to.deep.equal([]);
    });
});
