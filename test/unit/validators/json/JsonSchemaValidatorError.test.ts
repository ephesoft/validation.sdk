import { assert } from 'chai';
import { JsonSchemaValidatorError } from '../../../../src/validators/json/JsonSchemaValidatorError';

describe('JsonSchemaValidatorError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new JsonSchemaValidatorError('Test Message');
            assert.strictEqual(error.message, 'Test Message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'JsonSchemaValidatorError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('Inner Error');
            const error = new JsonSchemaValidatorError('Outer Error', innerError);
            assert.strictEqual(error.message, 'Outer Error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'JsonSchemaValidatorError');
        });
    });
});
