import { assert } from 'chai';
import { InvalidJsonSchemaError } from '../../../../src/validators/json/InvalidJsonSchemaError';

describe('InvalidJsonSchemaError unit tests', (): void => {
    describe('constructor(string, Error?)', (): void => {
        it('should correctly populate error object if only message supplied', (): void => {
            const error = new InvalidJsonSchemaError('Test Message');
            assert.strictEqual(error.message, 'Test Message');
            assert.strictEqual(error.innerError, null);
            assert.strictEqual(error.name, 'InvalidJsonSchemaError');
        });

        it('should correctly populate error object if message and inner error supplied', (): void => {
            const innerError = new Error('Inner Error');
            const error = new InvalidJsonSchemaError('Outer Error', innerError);
            assert.strictEqual(error.message, 'Outer Error');
            assert.deepStrictEqual(error.innerError, innerError);
            assert.strictEqual(error.name, 'InvalidJsonSchemaError');
        });
    });
});
