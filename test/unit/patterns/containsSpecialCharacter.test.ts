import { expect } from 'chai';

import { patterns } from '../../../src';

describe('ContainsSpecialCharacter: Pattern', (): void => {
    //*** Positive Test cases ***/

    it('should return true if it has special character', (): void => {
        expect(patterns.containsSpecialCharacter.test('@')).to.equal(true);
    });

    it('should return true if it has special character with other character as well', (): void => {
        const specialChars = ['~', '`', '!', '#', '$', '%', '^', '&', '*', '(', ')', '/', '<', '>', '=', '_'];
        specialChars.forEach((char): void => {
            expect(patterns.containsSpecialCharacter.test(`1${char}As`)).to.equal(true, `Testing for ${char}`);
        });
    });

    //** Negative cases */

    it('should return false if it doesn`t have special character', (): void => {
        expect(patterns.containsSpecialCharacter.test('Aa1 ')).to.equal(false);
    });
});
