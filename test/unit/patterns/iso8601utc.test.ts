import { expect } from 'chai';
import { patterns } from '../../../src';

describe('iso8601utc: Pattern', (): void => {
    //*** Positive Test cases ***/
    it('should validate a valid date', (): void => {
        expect('2021-03-30T11:42:01.670Z').to.match(patterns.iso8601utc);
    });

    it('should validate a date with zeros', (): void => {
        expect('2001-01-1T00:00:00.000Z').to.match(patterns.iso8601utc);
    });

    it('should validate a date with 2 digit subsecond', (): void => {
        expect('2001-01-1T00:00:00.00Z').to.match(patterns.iso8601utc);
    });

    //*** Negative Test cases ***/
    it('should not match date with invalid hour 25', (): void => {
        expect('2021-03-30T25:42:01.670Z').to.not.match(patterns.iso8601utc);
    });

    it('should not match date with invalid day 32', (): void => {
        expect('2021-03-32T11:42:01.670Z').to.not.match(patterns.iso8601utc);
    });

    it('should not match date with invalid month 13', (): void => {
        expect('2021-13-30T11:42:01.670Z').to.not.match(patterns.iso8601utc);
    });

    it('should not match date with missing UTC', (): void => {
        expect('2021-03-30T11:42:01.670').to.not.match(patterns.iso8601utc);
    });

    it('should not match date with missing Time', (): void => {
        expect('2021-03-30').to.not.match(patterns.iso8601utc);
    });

    it('should not match date with only time', (): void => {
        expect('11:42:01.670Z').to.not.match(patterns.iso8601utc);
    });
});
