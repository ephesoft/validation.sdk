import { expect } from 'chai';
import { patterns } from '../../../src/patterns/patterns';
import { ulid } from 'ulid';
import { v4 as uuidv4 } from 'uuid';

describe('patterns unit tests', function () {
    describe('id', function () {
        it('should accept valid ulid', function () {
            expect(patterns.id.test(ulid())).to.be.true;
        });

        it('should reject ulid in lower case', function () {
            expect(patterns.id.test(ulid().toLowerCase())).to.be.false;
        });

        it('should reject ULID with invalid character', function () {
            const invalidUlid = `~${ulid().substr(1)}`;
            expect(patterns.id.test(invalidUlid)).to.be.false;
        });

        it('should reject ULID that is too short', function () {
            const invalidUlid = ulid().substr(1);
            expect(patterns.id.test(invalidUlid)).to.be.false;
        });

        it('should reject ULID that is too long', function () {
            const invalidUlid = ulid() + 'A';
            expect(patterns.id.test(invalidUlid)).to.be.false;
        });

        it('should reject truncated UUID', function () {
            const invalidUlid = uuidv4().substr(0, 26).toUpperCase();
            expect(patterns.id.test(invalidUlid)).to.be.false;
        });

        it('should accept valid UUID', function () {
            expect(patterns.id.test(uuidv4())).to.be.true;
        });

        it('should accept valid UUID in upper case', function () {
            expect(patterns.id.test(uuidv4().toUpperCase())).to.be.true;
        });

        it('should reject UUID without dashes', function () {
            const invalidUuid = uuidv4().replace(/-/g, '');
            expect(patterns.id.test(invalidUuid)).to.be.false;
        });

        it('should reject UUID with invalid character', function () {
            // "g" is not valid
            const invalidUuid = `3ae4d6g5-71a6-4126-8091-dd17d20a4d0e`;
            expect(patterns.id.test(invalidUuid)).to.be.false;
        });

        it('should reject UUID that is too short', function () {
            const invalidUuid = `3ae4d6g5-71a6-4126-8091-dd17d20a4d0`;
            expect(patterns.id.test(invalidUuid)).to.be.false;
        });

        it('should reject UUID that is too long', function () {
            const invalidUuid = `3ae4d6g5-71a6-4126-8091-dd17d20a4d0e1`;
            expect(patterns.id.test(invalidUuid)).to.be.false;
        });
    });

    describe('ulid', function () {
        it('should accept valid ulids', function () {
            // Run a few to be sure...
            for (let i = 0; i < 100; i++) {
                expect(patterns.ulid.test(ulid())).to.be.true;
            }
        });

        it('should reject ULID with invalid character', function () {
            const invalidUlid = `~${ulid().substr(1)}`;
            expect(patterns.ulid.test(invalidUlid)).to.be.false;
        });

        it('should reject ULID that is too short', function () {
            const invalidUlid = ulid().substr(1);
            expect(patterns.ulid.test(invalidUlid)).to.be.false;
        });

        it('should reject ULID that is too long', function () {
            const invalidUlid = ulid() + 'A';
            expect(patterns.ulid.test(invalidUlid)).to.be.false;
        });
    });
});
