import { expect } from 'chai';

import { patterns } from '../../../src';

describe('StrongPassword: Pattern', (): void => {
    //*** Positive Test cases ***/

    it('should return true if it has strong password', (): void => {
        expect(patterns.strongPassword.test('Hello@123')).to.equal(true);
    });

    //** Negative cases */

    it('should fail if does not have at least one upper case in password', (): void => {
        expect(patterns.strongPassword.test('hello@123')).to.equal(false);
    });

    it('should fail if does not have at least one lower case in password', (): void => {
        expect(patterns.strongPassword.test('HELLO@123')).to.equal(false);
    });

    it('should fail if does not have at least one special character in password', (): void => {
        expect(patterns.strongPassword.test('HELLO1123')).to.equal(false);
    });

    it('should fail if does not have at least one containsDigit in password', (): void => {
        expect(patterns.strongPassword.test('loreumIpsum@')).to.equal(false);
    });

    it('should fail if does not have min length of 8', (): void => {
        expect(patterns.strongPassword.test('Hello@1')).to.equal(false);
    });

    it('should fail if the length goes more than 65', (): void => {
        const password = 'Hello@123loreumHello@123loreumHello@123loreumHello@123loreumHello@';
        expect(password.length).to.greaterThan(65);
        expect(patterns.strongPassword.test(password)).to.equal(false);
    });
});
