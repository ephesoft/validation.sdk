import { expect } from 'chai';

import { patterns } from '../../../src';

describe('ContainsLowerCase: Pattern', (): void => {
    //*** Positive Test cases ***/

    it('should return true if it has LowerCase character', (): void => {
        expect(patterns.containsLowerCase.test('a')).to.equal(true);
    });

    it('should return true in case of LowerCase character with other characters/number as well', (): void => {
        expect(patterns.containsLowerCase.test('0a@')).to.equal(true);
    });

    //** Negative cases */

    it('should return false if it doesn`t have lower case', (): void => {
        expect(patterns.containsLowerCase.test('A@  ;;1')).to.equal(false);
    });
});
