import { expect } from 'chai';

import { patterns } from '../../../src';

describe('ContainsDigit: Pattern', (): void => {
    //*** Positive Test cases ***/
    it('should return true in case of number', (): void => {
        expect(patterns.containsDigit.test('0123456789')).to.equal(true);
    });

    it('should return true in case of number with other characters as well', (): void => {
        expect(patterns.containsDigit.test('0A@')).to.equal(true);
    });

    //** Negative cases */

    it('should return false if it doesn`t have number', (): void => {
        expect(patterns.containsDigit.test('a@  ;;')).to.equal(false);
    });
});
