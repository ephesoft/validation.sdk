import { expect } from 'chai';
import { patterns } from '../../../src';

describe('URL: Pattern', function (): void {
    //*** Positive Test cases ***/

    it('should return true in case of valid url', function (): void {
        const validUrls = [
            'https://ephesoft.com/about/',
            'https://webhook-example.com/fakeid3421421',
            'https://www.google.co.uk/'
        ];

        validUrls.forEach((url): void => {
            expect(patterns.url.test(url)).to.equal(true, `Testing for ${url}`);
        });
    });

    it('should return true with dash in tld', function (): void {
        expect(patterns.url.test('https://ephesoft.l-b')).to.equal(true);
    });

    it('should return true with query string params', function (): void {
        expect(patterns.url.test('https://ephesoft.com?param=value')).to.equal(true);
    });

    it('should return true with port number', function (): void {
        expect(patterns.url.test('https://ephesoft.com:80')).to.equal(true);
    });

    //** Negative cases */

    it('should return false without protocol', function (): void {
        expect(patterns.url.test('whoneedsprotocols.com')).to.equal(false);
    });

    it('should return false with special characters in domain name', function (): void {
        const invalidUrls = ['https://ephe+soft.com', 'https://ephe_soft.com', 'https://ephe#soft.com', 'https://ephe*soft.com'];

        invalidUrls.forEach((url): void => {
            expect(patterns.url.test(url)).to.equal(false, `Testing for ${url}`);
        });
    });
});
