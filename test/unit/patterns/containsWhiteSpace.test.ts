import { expect } from 'chai';

import { patterns } from '../../../src';

describe('ContainsWhiteSpace: Pattern   ', (): void => {
    //*** Positive Test cases ***/
    it('should return true if it has white space', (): void => {
        expect(patterns.containsWhiteSpace.test('t e s t')).to.equal(true);
    });

    //** Negative cases */
    it('should return false if it doesn`t have white space', (): void => {
        expect(patterns.containsWhiteSpace.test('Aa1')).to.equal(false);
    });
});
