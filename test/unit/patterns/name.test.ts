import { expect } from 'chai';

import { patterns } from '../../../src';

describe('Name: Pattern', (): void => {
    //*** Positive Test cases ***/

    it('should allow valid name for name pattern', (): void => {
        const validNames = ['Quiñones', 'José Due', 'Björk', '毛泽东'];
        validNames.forEach((name): void => {
            expect(patterns.name.test(name)).to.equal(true, `Testing for ${name}`);
        });
    });

    //** Negative cases */

    it('should give error if the length is less than 2 for name pattern', (): void => {
        expect(patterns.name.test('s')).to.equal(false);
    });

    it('should give error if the length is greater than 30 for name pattern', (): void => {
        expect(patterns.name.test('loreumipsumloreumipsumloreumipsumloreumipsumloreumipsumloreumipsum')).to.equal(false);
    });

    it('should not allow number for name pattern', (): void => {
        expect(patterns.name.test('12345678910111213151617181920212223242526272829')).to.equal(false);
    });
});
