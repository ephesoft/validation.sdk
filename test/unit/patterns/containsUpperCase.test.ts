import { expect } from 'chai';

import { patterns } from '../../../src';

describe('ContainsUpperCase: Pattern', (): void => {
    //*** Positive Test cases ***/
    it('should return true if it has UpperCase character', (): void => {
        expect(patterns.containsUpperCase.test('A')).to.equal(true);
    });

    it('should return true in case of UpperCase character with other characters/number as well', (): void => {
        expect(patterns.containsUpperCase.test('0A@')).to.equal(true);
    });

    //** Negative cases */

    it('should return false if it doesn`t have upper case', (): void => {
        expect(patterns.containsUpperCase.test('a@  ;;1')).to.equal(false);
    });
});
