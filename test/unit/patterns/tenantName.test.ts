import { expect } from 'chai';

import { patterns } from '../../../src';

describe('TenantName: Pattern', function () {
    //*** Positive Test cases ***/

    it('should have a valid sub domain with min length 3 and max length 50', function () {
        expect(patterns.tenantName.test('myTenant-test1')).to.be.true;
        expect(patterns.tenantName.test('myT')).to.be.true;
        expect(patterns.tenantName.test('myinLongerWordsSomemoreLongerWthLoreumIpsummaxin45')).to.be.true;
    });

    it('should allow lower case', function () {
        expect(patterns.tenantName.test('mytenant')).to.be.true;
    });

    it('should allow upper case', function () {
        expect(patterns.tenantName.test('MYTENANT')).to.be.true;
    });

    it('should allow hyphen in between character', function () {
        expect(patterns.tenantName.test('my-tenant')).to.be.true;
    });

    it('should allow multiple hyphen in in repeat ', function () {
        expect(patterns.tenantName.test('my-tenant-domain')).to.be.true;
    });

    //** Negative cases */

    it('should give false is the sub domain length is less than 3 characters', function () {
        expect(patterns.tenantName.test('m')).to.be.false;
        expect(patterns.tenantName.test('mm')).to.be.false;
        expect(patterns.tenantName.test('')).to.be.false;
    });

    it('should give false is the sub domain length is more than 50 characters', function () {
        expect(patterns.tenantName.test('myinLongerWordsSomemoreLongerWthLoreumIpsummaxin456')).to.be.false;
    });

    it('should return false in case of any special characters except hyphen', function () {
        const specialChars = ['~', '`', '!', '#', '$', '%', '^', '&', '*', '(', ')', '/', '<', '>', '=', '_'];
        specialChars.forEach((char) => {
            expect(patterns.tenantName.test(`mydoMain${char}Tenant`)).to.be.false;
        });
    });

    it('should return false in case of any white space', function () {
        expect(patterns.tenantName.test('my domain')).to.be.false;
    });

    it('should not allow hyphen only', function () {
        expect(patterns.tenantName.test('-')).to.be.false;
    });

    it('should not have hyphen in the start', function () {
        expect(patterns.tenantName.test('-tenant')).to.be.false;
    });

    it('should not allow hyphen in the end', function () {
        expect(patterns.tenantName.test('domain-')).to.be.false;
    });

    it('should not allow number as first character', function () {
        expect(patterns.tenantName.test('1ab')).to.be.false;
    });

    it('should not allow only numbers', function () {
        expect(patterns.tenantName.test('999')).to.be.false;
    });
});
