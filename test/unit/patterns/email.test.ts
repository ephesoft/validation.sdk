import { expect } from 'chai';

import { patterns } from '../../../src';

describe('Email: Pattern', (): void => {
    //*** Positive Test cases ***/

    it('should return true in case valid email', (): void => {
        const validEmails = [
            'customer@gmail.com',
            'customer.support@gmail.com',
            'cu@gmail.co.in',
            'customer93@gmail.com',
            'customer.supp93@gmail.com',
            'customer@gmailLoreumIpsum.co.in',
            'customer@m3n.co.in',
            'customer@ephe-soft.com'
        ];
        validEmails.forEach((email): void => {
            expect(patterns.email.test(email)).to.equal(true, `Testing for ${email}`);
        });
    });

    it('should return true with special characters in username', (): void => {
        const validEmails = ['*@ephesoft.com', 'user+random@ephesoft.com'];
        validEmails.forEach((email): void => {
            expect(patterns.email.test(email)).to.equal(true, `Testing for ${email}`);
        });
    });

    //** Negative cases */

    it('should return false without username', (): void => {
        expect(patterns.email.test('@gmail.com')).to.equal(false);
    });

    it('should return false with wildcard in domain name', (): void => {
        expect(patterns.email.test('user@*.com')).to.equal(false);
    });

    it('should return false with special characters in domain name', (): void => {
        const invalidEmails = ['user@ephe+soft.com', 'user@ephe_soft.com', 'user@ephe#soft.com'];
        invalidEmails.forEach((email): void => {
            expect(patterns.email.test(email)).to.equal(false, `Testing for ${email}`);
        });
    });

    it('should return false with special characters in username', (): void => {
        const invalidEmails = ['user/example@example.com', 'user\\/example@example.com', "123;'[';./,.`@fd@fd@example.com"];
        invalidEmails.forEach((email): void => {
            expect(patterns.email.test(email)).to.equal(false, `Testing for ${email}`);
        });
    });
});
