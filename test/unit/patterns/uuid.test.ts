import { expect } from 'chai';

import { patterns } from '../../../src';

describe('UUID: Pattern', (): void => {
    //*** Positive Test cases ***/
    it('should have a valid UUID', (): void => {
        const validUUIDs = [
            'a60474ce-6a61-11ea-bc55-0242ac130003',
            '11111111-1111-1111-1111-111111111111',
            'AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA',
            'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa',
            '11111111-aaaa-AAAA-1Aab-BBBBBBBBBBBB'
        ];
        validUUIDs.forEach((uuid): void => {
            expect(patterns.uuid.test(uuid)).to.equal(true, `Testing for ${uuid}`);
        });
    });

    it('should only have Hex char range with number and hyphens', (): void => {
        const uuid = 'a60474ce-6a61-11ea-bc55-0242ac130003';
        expect(patterns.uuid.test(uuid)).to.equal(true);
    });

    //*** Negative Test cases ***/
    it('should return error if length is not equal to 36 including hyphen', (): void => {
        const uuid = 'a-a-aaa-a-aaa';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should return error if first part is not having a length of 8', (): void => {
        const uuid = 'a604-FFFF-FFFF-FFFF-FFFFFFFFFFFF';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should return error if second part is not having a length of 4', (): void => {
        const uuid = 'FFFFFFFF-a-FFFF-FFFF-FFFFFFFFFFFF';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should return error if third part is not having a length of 4', (): void => {
        const uuid = 'FFFFFFFF-FFFF-a-FFFF-FFFFFFFFFFFF';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should return error if fourth part is not having a length of 4', (): void => {
        const uuid = 'FFFFFFFF-FFFF-FFFF-a-FFFFFFFFFFFF';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should return error if fifth part is not having a length of 12', (): void => {
        const uuid = 'FFFFFFFF-FFFF-FFFF-FFFF-a';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });

    it('should not have white space', (): void => {
        expect(patterns.uuid.test('FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFF FF')).to.equal(false);
    });

    it('should return error if no hyphens', (): void => {
        expect(patterns.uuid.test('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF')).to.equal(false);
    });

    it('should give error if it is out of hex range', (): void => {
        const uuid = 'a60474cG-6a61-11ea-bc55-0242ac130003';
        expect(patterns.uuid.test(uuid)).to.equal(false);
    });
});
