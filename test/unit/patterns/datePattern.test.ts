import { expect } from 'chai';
import {
    datePattern,
    DatePatternOptions,
    Format,
    MatchType,
    Parts,
    Precision,
    TimeZone
} from '../../../src/patterns/datePattern';

describe('datePattern', (): void => {
    describe('works with options', (): void => {
        const date = new Date('2021-03-26T22:19:01.670Z');
        const testDate = date.toISOString();
        const options: DatePatternOptions = {
            date
        };
        it('no options', (): void => {
            const testDate2 = new Date(datePattern().toString().slice(2, -2)); // Remove ^ and $
            expect(testDate2.toISOString()).to.match(datePattern({ precision: Precision.Second }));
        });

        it('empty options', (): void => {
            const testDate2 = new Date(datePattern().toString().slice(2, -2)); // Remove ^ and $
            expect(testDate2.toISOString()).to.match(datePattern({ precision: Precision.Second }));
        });

        it('format', (): void => {
            const localOptions = { ...options, format: Format.Iso8601 };
            expect(testDate).to.match(datePattern(localOptions));
        });

        it('timezone', (): void => {
            const localOptions = { ...options, timeZone: TimeZone.Utc };
            expect(testDate).to.match(datePattern(localOptions));
        });

        it('parts -> Date', (): void => {
            const localOptions = { ...options, parts: Parts.Date, matchType: MatchType.Contains };
            expect(testDate).to.match(datePattern(localOptions));
        });

        it('parts -> Time', (): void => {
            const localOptions = { ...options, parts: Parts.Time, matchType: MatchType.Contains };
            expect(testDate).to.match(datePattern(localOptions));
        });
    });

    describe('dateTime', (): void => {
        describe('succesfully validates', (): void => {
            const date = new Date('2021-03-26T22:19:01.670Z');
            const options: DatePatternOptions = {
                parts: Parts.DateTime,
                date
            };
            it('exact date', (): void => {
                const testDate = date.toISOString();
                expect(testDate).to.match(datePattern(options));
            });

            it('second precision', (): void => {
                options.precision = Precision.Second;
                const testDate = '2021-03-26T22:19:01.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('minute precision', (): void => {
                options.precision = Precision.Minute;
                const testDate = '2021-03-26T22:19:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('hour precision', (): void => {
                options.precision = Precision.Hour;
                const testDate = '2021-03-26T22:00:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('day precision', (): void => {
                options.precision = Precision.Day;
                const testDate = '2021-03-26T00:00:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('month precision', (): void => {
                options.precision = Precision.Month;
                const testDate = '2021-03-01T00:00:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('year precision', (): void => {
                options.precision = Precision.Year;
                const testDate = '2021-01-01T00:00:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('no precision', (): void => {
                options.precision = Precision.None;
                const testDate = '2001-01-01T00:00:00.000Z';
                expect(testDate).to.match(datePattern(options));
            });

            it('contained date', (): void => {
                options.precision = Precision.None;
                options.matchType = MatchType.Contains;
                const testDate = `date: ${date.toISOString()}`;
                expect(testDate).to.match(datePattern(options));
            });
        });

        describe('properly errors', (): void => {
            const date = new Date('2021-03-26T22:19:01.670Z');
            const options: DatePatternOptions = {
                parts: Parts.DateTime,
                date
            };
            it('exact date', (): void => {
                const testDate = '2021-03-26T22:19:01.600Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('second precision', (): void => {
                options.precision = Precision.Second;
                const testDate = '2021-03-26T22:19:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('minute precision', (): void => {
                options.precision = Precision.Minute;
                const testDate = '2021-03-26T22:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('hour precision', (): void => {
                options.precision = Precision.Hour;
                const testDate = '2021-03-26T00:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('day precision', (): void => {
                options.precision = Precision.Day;
                const testDate = '2021-03-01T00:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('month precision', (): void => {
                options.precision = Precision.Month;
                const testDate = '2021-01-01T00:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('year precision', (): void => {
                options.precision = Precision.Year;
                const testDate = '2001-01-01T00:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('no precision', (): void => {
                options.precision = Precision.None;
                const testDate = '200-01-01T00:00:00.000Z';
                expect(testDate).to.not.match(datePattern(options));
            });

            it('contained date', (): void => {
                options.precision = Precision.None;
                const testDate = `date: ${date.toISOString()}`;
                expect(testDate).to.not.match(datePattern(options));
            });
        });
    });
});
