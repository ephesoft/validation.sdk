# Changelog

## 1.16.0 (2022-10-20)
- Removed internal package references

## 1.15.0 (2022-10-20)
- Updated packages
- Updated publish to NPM
- Updated README.md with more documentation

## 1.14.0 (2022-10-03)
- Updated to template version 6.0.2

## 1.13.1 (2022-05-09)
- Fixed `lodash.clonedeep` invalid dependency name casing

## 1.13.0 (2022-03-08)
- Updated patterns.tenantName to only allow up to 50 characters

## 1.12.0 (2021-11-17)
- Added pattern.url for validating urls

## 1.11.0 (2021-11-04)
- Added pattern.id that will take either a ULID or a UUID

## 1.10.0 (2021-11-04)
- Added pattern.ulid regular expression (found pattern here: https://github.com/zalando/skipper/blob/master/filters/flowid/ulid.go)

## 1.9.0 (2021-08-05)
- Updated email pattern to be more restrictive

## 1.8.1 (2021-06-09)
- Fixed index so it includes JsonSchemaValidationFailure interface
- Fixed JsonSchemaValidator so schema is pre-compiled at instantiation
- Added context information to validation results
- Fixed audit issues
- Updated npm references

## 1.8.0 (2021-05-25)
- Added JsonSchemaValidator class to standardize the way we validate JSON against a schema
- Fixed output when generating API documentation locally
- Updated npm references

## 1.7.0 (2021-03-26)
- Added datePattern
- Added changelog tests

## 1.6.0 (2021-03-01)
- Updated tenantName pattern to only allow letters for first character.
- Updated packages

## 1.5.0 (2020-08-27)
- Updated npm references and fixed linting errors

## 1.4.1 (2020-08-11)
- Fixed Publish command

## 1.4.0 (2020-08-11)
- Updated up email validation to only allow RFC1123 compliant domain names
- Updated package to implement some of the latest standards
- Fixed linting issues

## 1.3.1 (2020-04-02)
- Fixed: isValidNumber should validate both strings and numbers

## 1.3.0 (2020-03-27)
- Added InvoiceFieldValidators for dates and numbers
- Added pattern for numbers
- Added library for iso-8601 date validation
- Updated validator tests to be in one folder

## 1.2.0 (2020-03-20)
- Added pattern for uuid

## 1.1.0 (2020-03-17)
- Added domain pattern
- Updated file path in index.ts

## 1.0.0 (2020-03-05)
- Added custom validator and patterns

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)
