# README #
# @ephesoft/validation.sdk
This library contains custom validators and patterns.

__IMPORTANT__: _@ephesoft/core.sdk is required as a peer dependency when using classes under the validators/json namespace._

[TOC]

## Using the library

### Prerequisites

Add the package to your repository:
```bash
npm i @ephesoft/validation.sdk
```

### Using the library
Using the patterns:
```typescript
// Import Library
import { patterns } from '@ephesoft/validation.sdk';
import { expect } from 'chai'; // Used for example

// Check ISO 8601 date
expect('2001-01-1T00:00:00.000Z').to.match(patterns.iso8601utc);

// Check URL
expect(patterns.url.test('whoneedsprotocols.com')).to.equal(false);

// Check UUID
expect(patterns.uuid.test('a60474ce-6a61-11ea-bc55-0242ac130003')).to.equal(true);

// Many more patterns....
```

Using the JSON Schema Validator ([JSON Schema](https://json-schema.org/)):
```typescript
// Import Library
import { JsonSchemaValidator } from '@ephesoft/validation.sdk/validators/json/JsonSchemaValidator';
import { expect } from 'chai'; // Used for example

// Validate Success - empty array
const validator = new JsonSchemaValidator({ type: 'object' });
expect(validator.validate({})).to.deep.equal([]);

// Validation Issues - See response for each issue
const validator = new JsonSchemaValidator({
    type: 'object',
    properties: {
        key: { type: 'string' }
    },
    required: ['key']
});
expect(validator.validate({ key: 42 })).to.deep.equal([
    {
        code: 'type',
        message: 'must be string',
        location: {
            objectPath: '/key',
            schemaPath: '#/properties/key/type'
        },
        context: {
            type: 'string'
        }
    }
]);
```

## NPM run targets
Run NPM commands as
```bash
npm run command
```

### Available Commands
|Command|Description|
|:-|:-|
| audit | Executes a dependency audit using the default NPM registry |
| audit:fix | Attempts an automatic fix of any audit failures. |
| build | Cleans the dist folder and transpiles/webpacks the code |
| clean | Deletes the working folders like dist and docs |
| clean:docs | Deletes the docs folder |
| create:beta | Auto versions the package to the next available beta version. |
| docs | Create API documentation for the package in a "docs" folder |
| docs:bitbucket | Creates API documentation in Bitbucket markdown format in a "docs" folder |
| lint | Performs static analysis on the TypeScript files. |
| lint:fix | Performs static analysis on the TypeScript files and attempts to automatically fix errors. |
| lint:report | Performs static analysis on the TypeScript files using the default validation rules and logs results to a JSON file. |
| pack | Creates a local tarball package |
| postbuild| Automatically ran during build. |
| postprepare  | Automatically ran during package install to setup tooling. |
| prebuild | Automatically ran during build. |
| start | Instructs the TypeScript compiler to compile the ts files every time a change is detected. |
| test | Executes all tests and generates coverage. |
| test:coverage | Generates code coverage reports from test data. |
| test:unit | Runs the unit tests. |
| validate:ga | Validates the package version is valid for GA release. |
| validate:beta | Validates the package version is valid for a Beta release. |