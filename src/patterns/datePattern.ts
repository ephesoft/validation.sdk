export enum Format {
    Iso8601 = 'iso8601'
}

export enum Parts {
    Date = 'date',
    DateTime = 'datetime',
    Time = 'time'
}

export enum Precision {
    Exact,
    Second,
    Minute,
    Hour,
    Day,
    Month,
    Year,
    None
}

export enum TimeZone {
    Utc = 'Z' // Must be capitalized for use in date match
}

export enum MatchType {
    Full = 'full',
    Contains = 'contains'
}

export interface DatePatternOptions {
    date?: Date;
    format?: Format;
    precision?: Precision;
    parts?: Parts;
    timeZone?: TimeZone;
    matchType?: MatchType;
}

/**
 * @summary Function used to build a regex for validating date strings in tests.  Not built for production code.
 * @description Creates a regex pattern based on the precision specified to the current or passed in date.
 * This solution has a gap for real validation as it doesn't handle actual date ranges.
 * Your test date could be less than a second later than the pattern date but still be in a different day, month etc...
 * A proper future solution would build a date validator that checks ranges to truly have 1 second, 1 minute, 1 hour etc. precision.
 */
export function datePattern(options?: DatePatternOptions): RegExp {
    // Set Default Options
    options = options ?? {};
    const date = options.date ?? new Date();
    const format = options.format ?? Format.Iso8601;
    const parts = options.parts ?? Parts.DateTime;
    const precision = options.precision ?? Precision.Exact;
    const timeZone = options.timeZone ?? TimeZone.Utc;
    const matchType = options.matchType ?? MatchType.Full;

    if (format !== Format.Iso8601) {
        throw new Error(`Date format: ${format} - not supported`);
    }

    if (timeZone !== TimeZone.Utc) {
        throw new Error(`Date timeZone: ${timeZone} - not supported`);
    }

    let pattern = '';
    switch (parts) {
        case Parts.DateTime:
            pattern = `${dateISOPattern(date, precision)}T${timeISOPattern(date, precision)}${timeZone}`;
            break;
        case Parts.Date:
            pattern = dateISOPattern(date, precision);
            break;
        case Parts.Time:
            pattern = timeISOPattern(date, precision);
            break;
        default:
            throw new Error(`Date parts: ${parts} - not supported`);
    }

    if (matchType === MatchType.Full) {
        return RegExp(`^${pattern}$`);
    }
    return RegExp(pattern);
}

function timeISOPattern(date: Date, precision: Precision): string {
    // Pattern: Hour, Minute, Second, milliseconds
    const pattern = ['(?:[01]?[0-9]|2[0-3])', '[0-5][0-9]', '[0-5][0-9]', '\\d+'];

    // Remove the date and timezone
    // Example: 2021-03-26T22:19:01.670Z
    const timeParts = date.toISOString().split('T')[1].replace('Z', '').split(':');

    // Swap generic pattern with specific times based on precision
    switch (true) {
        case precision === Precision.Exact:
            pattern[3] = timeParts[2].split('.')[1];
        // fall through
        case precision <= Precision.Second:
            pattern[2] = timeParts[2].split('.')[0];
        // fall through
        case precision <= Precision.Minute:
            pattern[1] = timeParts[1];
        // fall through
        case precision <= Precision.Hour:
            pattern[0] = timeParts[0];
    }

    return `${pattern.slice(0, 3).join(':')}.${pattern[3]}`;
}

function dateISOPattern(date: Date, precision: Precision): string {
    // Pattern: Year, Month, Day
    const pattern = ['\\d{4}', '(?:[0]?[1-9]|1[0-2])', '(?:[0-2]?[0-9]|3[01])'];

    // Remove the date and timezone
    // Example: 2021-03-26T22:19:01.670Z
    const timeParts = date.toISOString().split('T')[0].split('-');

    // Swap generic pattern with specific times based on precision
    switch (true) {
        case precision <= Precision.Day:
            pattern[2] = timeParts[2];
        // fall through
        case precision <= Precision.Month:
            pattern[1] = timeParts[1];
        // fall through
        case precision <= Precision.Year:
            pattern[0] = timeParts[0];
    }

    return pattern.join('-');
}
