/**
 * @property {regex}  email                 - Regex for valid email.  https://tools.ietf.org/html/rfc1123
 * Description:(/^[a-zA-Z0-9_+.*&?'=^{}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.-]+$/)
 * Start anchor (^)
 * Start with one or more characters ([a-zA-Z0-9-_+.*&?'=^{}~]+) https://www.jochentopf.com/email/chars.html
 * Next contain the @ sign (@)
 * Next have one or more valid characters RFC1123. Alphanumeric or dash ([a-zA-Z0-9-]+)
 * Next contain the . sign (\.)
 * Next have one or more valid characters RFC1123. Alphanumeric, period or dash ([a-zA-Z0-9\.-]+)
 * End anchor ($)
 * @property {regex}  name                  - Regex for valid name with some uni codes
 * Description:
 * This will accept any A-Z or a-z or Latin or
 * han script (chinese and japanese) http://www.unicode.org/charts/unihan.html
 * we can verify this over unicode.org
 *
 * [\u2e80-\u2fd5]
 * CJK Radicals Supplement is a Unicode block containing alternative, often positional, forms of the Kangxi radicals. They are used headers in dictionary indices and other CJK ideograph collections organized by radical-stroke.
 *
 * [\u3190-\u319f]
 * Kanbun is a Unicode block containing annotation characters used in Japanese copies of classical Chinese texts, to indicate reading order.
 *
 * [\u3400-\u4DBF]
 * CJK Unified Ideographs Extension-A is a Unicode block containing rare Han ideographs.
 *
 * [\u4E00-\u9FCC]
 * CJK Unified Ideographs is a Unicode block containing the most common CJK ideographs used in modern Chinese and Japanese.
 *
 * [\uF900-\uFAAD]
 * CJK Compatibility Ideographs is a Unicode block created to contain Han characters that were encoded in multiple locations in other established character encodings, in addition to their CJK Unified Ideographs assignments, in order to retain round-trip compatibility between Unicode and those encodings.
 *
 * @property {regex}  strongPassword        - Regex for strong password with following patterns.
 * Description
 * Should have at least one uppercase ?=.*[A-Z]
 * Should have at least one lowercase ?=.*[a-z])
 * Should have at least one number (?=.*\d)
 * Should have one special character from !"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~
 * Should have min length of 8 characters
 * Should have max length of 65 characters
 *
 * @property {regex}  containsWhiteSpace            - Regex for white space.
 * @property {regex}  containsDigit                 - Regex for digit.
 * @property {regex}  containsUpperCase             - Regex for uppercase.
 * @property {regex}  containsLowerCase             - Regex for lowercase.
 * @property {regex}  containsSpecialCharacter      - Regex for special char.
 * @property {regex}  tenantName                    - Regex for tenantName.
 * Description subdomain
 * https://regexper.com/#%5E%5BA-Za-z0-9%5D%7B1%7D%5BA-Za-z0-9-%5D%7B1%2C59%7D%5BA-Za-z0-9%5D%7B1%7D%24
 * @property {regex}  uuid                          - Regex for UUID
 * Description
 * https://regexper.com/#%2F%5E%5B0-9a-fA-F%5D%7B8%7D-%5B0-9a-fA-F%5D%7B4%7D-%5B0-9a-fA-F%5D%7B4%7D-%5B0-9a-fA-F%5D%7B4%7D-%5B0-9a-fA-F%5D%7B12%7D%24%2F
 * @property {regex} number
 * Description
 *  used to validate negative/positive numbers with/without decimals
 * @property {regex}  iso8601utc                    - Regex for validating iso 8601 dates in UTC.  Example Date: 2021-03-26T22:19:01.670Z
 */
export const patterns = {
    email: /^[a-zA-Z0-9_+.*&?'=^{}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9.-]+$/,
    name: /^[A-Za-z\u00C0-\u00FF\u2E80-\u2FD5\u3190-\u319f\u3400-\u4DBF\u4E00-\u9FCC\uF900-\uFAADU\u0027\u0022`_ .-]{2,30}$/,
    strongPassword: /^(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~])(?=.*[a-z])(?=.*[A-Z]).{8,65}$/,
    containsWhiteSpace: /\s/,
    containsDigit: /\d/,
    containsUpperCase: /[A-Z]/,
    containsLowerCase: /[a-z]/,
    containsSpecialCharacter: /[!"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~]/,
    tenantName: /^[A-Za-z]{1}[A-Za-z0-9-]{1,48}[A-Za-z0-9]{1}$/,
    id: /(^[0123456789ABCDEFGHJKMNPQRSTVWXYZ]{26}$)|(^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$)/,
    ulid: /^[0123456789ABCDEFGHJKMNPQRSTVWXYZ]{26}$/,
    uuid: /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/,
    number: /(^(-{0,1})[0-9]+(\.[0-9]+){0,1}$)|(^(-{0,1})\.[0-9]+$)/,
    iso8601utc: /^\d{4}-(?:[0]?[1-9]|1[0-2])-(?:[0-2]?[0-9]|3[01])T(?:[01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]\.\d+Z$/,
    url: /(https?:\/\/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9])(:?\d*)\/?([a-z_\\/0-9\-#.]*)\??([a-z_\\/0-9\-#=&]*)/
};
