import * as isoDatestringValidator from 'iso-datestring-validator';
import { patterns } from '../patterns/patterns';

interface ValidateIsoDateFlags {
    /** If this is set to true separators will be ignored */
    ignoreSeparators: boolean;
    separator?: string;
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class InvoiceFieldValidators {
    /**
     * Validates ISO-8601 dates only with format (yyyy-mm-dd) or other separators
     * @param date valid ISO-8601 string (yyyy-mm-dd)
     * @param flags iso flags to validate different separators (separator could be enum)
     */
    public static isValidIsoDate(date: string, flags?: ValidateIsoDateFlags): boolean {
        if (flags?.ignoreSeparators) {
            /** Remove separators. Strip any chars but digits and validate on string */
            const strippedDate = date.replace(/[^0-9]/g, '');
            return isoDatestringValidator.isValidDate(strippedDate, '');
        }

        if (typeof flags?.separator !== 'undefined') {
            return isoDatestringValidator.isValidDate(date, flags.separator);
        }

        /** default is dash but adding explictly for visibility*/
        return isoDatestringValidator.isValidDate(date, '-');
    }

    /**
     * Used to validate negative and positive numbers with/without decimals
     * @param value Valid number as string or number
     */
    public static isValidNumber(value: string | number): boolean {
        return patterns.number.test(`${value}`);
    }
}
