import { NestedError } from '@ephesoft/core.sdk/errors/NestedError';

/**
 * Indicates an internal failure in a [[JsonSchemaValidator]] instance.
 */
export class JsonSchemaValidatorError extends NestedError {
    /**
     * Initializes a new instance of the [JsonSchemaValidatorError]] class.
     * @param message Description of the error.
     * @param innerError Error that caused the [[JsonSchemaValidatorError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'JsonSchemaValidatorError';
    }
}
