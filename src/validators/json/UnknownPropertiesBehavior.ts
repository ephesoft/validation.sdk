/**
 * Specifies how unknown properties should be treated during JSON validation.
 */
export enum UnknownPropertiesBehavior {
    /**
     * In cases where the JSON schema doesn't explicitly specify behavior, unknown properties
     * are prohibited.
     */
    ProhibitUnlessExplicitlyAllowed = 'prohibitunlessexplicitlyallowed',

    /**
     * ALL unknown properties are prohibited regardless of JSON schema settings.
     */
    ProhibitAll = 'prohibitall',

    /**
     * In cases where the JSON schema doesn't explicitly specify behavior, unknown properties
     * are allowed.
     */
    AllowUnlessExplicitlyProhibited = 'allowunlessexplicitlyprohibited',

    /**
     * ALL unknown properties are allowed regardless of JSON schema settings.
     */
    AllowAll = 'allowall'
}
