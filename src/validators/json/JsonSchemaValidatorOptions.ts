import { UnknownPropertiesBehavior } from './UnknownPropertiesBehavior';

/**
 * Options for creating a [[JsonSchemaValidator]] instance.
 */
export interface JsonValidatorOptions {
    /**
     * Specifies how unknown properties should be treated during JSON validation.
     *
     * Defaults to [[UnknownPropertiesBehavior.AllowUnlessExplicitlyProhibited]].
     */
    unknownPropertiesBehavior?: UnknownPropertiesBehavior;
}
