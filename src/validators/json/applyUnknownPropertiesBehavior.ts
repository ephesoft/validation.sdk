import { UnknownPropertiesBehavior } from './UnknownPropertiesBehavior';
import { InvalidParameterError } from '@ephesoft/core.sdk/errors/InvalidParameterError';
import cloneDeep = require('lodash.clonedeep');

/**
 * Creates a clone of the specified schema with additionalProperties set according to the input parameters.
 * @param schemaObject The schema object to be cloned.
 * @returns The cloned schema object.
 */
export function applyUnknownPropertiesBehavior(
    schemaObject: object,
    unknownPropertiesBehavior: UnknownPropertiesBehavior
): object {
    // AJVs schema validation should weed out circular references, so we won't worry about them here
    function applyUknownPropertiesBehaviorRecursive(target: object): object {
        const mutableSchemaObject = (target ?? {}) as Record<string, unknown>;
        const type = getType(mutableSchemaObject);
        if (type === 'object') {
            switch (unknownPropertiesBehavior) {
                case UnknownPropertiesBehavior.AllowUnlessExplicitlyProhibited:
                    if (!('additionalProperties' in mutableSchemaObject)) {
                        mutableSchemaObject.additionalProperties = true;
                    }
                    break;

                case UnknownPropertiesBehavior.AllowAll:
                    mutableSchemaObject.additionalProperties = true;
                    break;

                case UnknownPropertiesBehavior.ProhibitAll:
                    mutableSchemaObject.additionalProperties = false;
                    break;

                case UnknownPropertiesBehavior.ProhibitUnlessExplicitlyAllowed:
                    if (!('additionalProperties' in mutableSchemaObject)) {
                        mutableSchemaObject.additionalProperties = false;
                    }
                    break;

                default:
                    throw new InvalidParameterError(
                        'unknownPropertiesBehavior',
                        `Unsupported unknown properties behavior: "${unknownPropertiesBehavior}"`
                    );
            }

            const properties = (mutableSchemaObject.properties ?? {}) as Record<string, object>;
            let propertyName: keyof typeof properties;
            for (propertyName in properties) {
                applyUknownPropertiesBehaviorRecursive(properties[propertyName]);
            }
        } else if (type === 'array') {
            applyUknownPropertiesBehaviorRecursive(mutableSchemaObject.items as object);
        } else if (Array.isArray(target)) {
            for (const item of target as object[]) {
                applyUknownPropertiesBehaviorRecursive(item);
            }
        } else if (typeof target === 'object') {
            const targetObject = target as Record<string, object>;
            let propertyName: keyof typeof targetObject;
            for (propertyName in target) {
                applyUknownPropertiesBehaviorRecursive(targetObject[propertyName]);
            }
        }

        return target;
    }

    return applyUknownPropertiesBehaviorRecursive(cloneDeep(schemaObject));
}

/**
 * Gets the value of the type property as a string.
 * @param schemaObject The schema object to get the type from.
 * @returns The value of the type property as a string - this will be 'unknown' if the type property is missing
 *          or is a union type.
 */
function getType(schemaObject: Record<string, unknown>): string {
    if (typeof schemaObject.type === 'string') {
        return schemaObject.type;
    }
    if (Array.isArray(schemaObject.type) && schemaObject.type.length === 1) {
        return String(schemaObject.type[0]);
    }
    return 'unknown';
}
