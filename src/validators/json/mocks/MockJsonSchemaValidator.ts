import { JsonSchemaValidator } from '../JsonSchemaValidator';
import { JsonSchemaValidationFailure } from '../JsonSchemaValidationFailure';
import { MockEngine } from '@ephesoft/mock.engine';
import { BaseReturnType, ExpectationOptions, MockOptions, Synchronous } from '@ephesoft/mock.engine';

type MockedFunctions = Pick<JsonSchemaValidator, 'validate'>;
/**
 * Mock implementation of [[JsonSchemaValidator]].
 */
export class MockJsonSchemaValidator extends JsonSchemaValidator {
    readonly #mockEngine: MockEngine<MockedFunctions>;

    /**
     * Initializes a new instance of the [[MockJsonSchemaValidator]] class.
     * @param options Options to be applied when creating the [[MockJsonSchemaValidation]] instance.
     */
    public constructor(options?: MockOptions) {
        super({ type: 'object' });
        // Note that the engine needs to know the name of this mock class
        this.#mockEngine = new MockEngine<MockedFunctions>('MockJsonSchemaValidator', options);
    }

    // #region mocked functions

    /**
     * Emulates a call to validate.
     * @param json The JSON object to be validated.
     * @returns The configured result.
     * @throws MockError If the function is called without first calling [[expectCall]] or
     *                   [[expectCalls]].
     */
    public validate(json: unknown): JsonSchemaValidationFailure[] {
        return this.#mockEngine.processCall('validate', [json]);
    }

    // #endregion mocked functions

    // #region boilerplate

    /**
     * Resets the [[MockJsonSchemaValidator]] instance back to a freshly-initialized state.
     */
    public reset(): MockJsonSchemaValidator {
        this.#mockEngine.reset();
        return this;
    }

    /**
     * Gets the calls made to the specified function.
     * @param functionName The name of the function to retrieve calls for.
     * @returns The calls made to the specified function.
     */
    public getCalls<T extends keyof MockedFunctions>(functionName: T): Parameters<MockedFunctions[T]>[] {
        return this.#mockEngine.getCalls(functionName);
    }

    /**
     * Sets an expectation that a call will be made to the specified function.
     * @param functionName The name of the function.
     * @param response The response to be returned from the function.
     * @param options Additional options to be applied when the function is called.
     */
    public expectCall<T extends keyof MockedFunctions>(
        functionName: T,
        response?: BaseReturnType<MockedFunctions[T]> | Error | Synchronous<MockedFunctions[T]>,
        options?: ExpectationOptions
    ): MockJsonSchemaValidator {
        this.#mockEngine.expectCall(functionName, response, options);
        return this;
    }

    /**
     * Sets an expectation that a number of calls will be made to the specified function.
     * @param functionName The name of the function.
     * @param responses The responses to be returned from the function.
     */
    public expectCalls<T extends keyof MockedFunctions>(
        functionName: T,
        ...responses: (BaseReturnType<MockedFunctions[T]> | Error | Synchronous<MockedFunctions[T]>)[]
    ): MockJsonSchemaValidator {
        this.#mockEngine.expectCalls(functionName, ...responses);
        return this;
    }

    // #endregion boilerplate
}
