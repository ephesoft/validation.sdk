import { applyUnknownPropertiesBehavior } from './applyUnknownPropertiesBehavior';
import { JsonValidatorOptions } from './JsonSchemaValidatorOptions';
import { UnknownPropertiesBehavior } from './UnknownPropertiesBehavior';
import { InvalidJsonSchemaError } from './InvalidJsonSchemaError';
import Ajv, { ErrorObject, ValidateFunction } from 'ajv';
import { JsonSchemaValidationFailure } from './JsonSchemaValidationFailure';
import { JsonSchemaValidatorError } from './JsonSchemaValidatorError';
import { InvalidParameterError } from '@ephesoft/core.sdk/errors/InvalidParameterError';

/**
 * Validates JSON objects against a JSON schema.
 */
export class JsonSchemaValidator {
    readonly #validate: ValidateFunction<unknown>;

    /**
     * Initializes a new instance of the [[JsonSchemaValidator]] class.
     * @param schema The schema to validate objects against.
     * @param options The options to be applied while validating JSON objects.
     * @throws [[InvalidJsonSchemaError]] If the supplied schema is invalid.
     */
    public constructor(schema: object, options?: JsonValidatorOptions) {
        this.#validate = JsonSchemaValidator.createValidator(schema, options);
    }

    /**
     * Validates the specified JSON object.
     * @param json The JSON object to be validated.
     * @returns An array of validation failures.  This will be an empty array
     *          if the JSON object was valid.
     * @throws [[JsonSchemaValidatorError]] If a critical internal error is encountered.
     */
    public validate(json: unknown): JsonSchemaValidationFailure[] {
        try {
            if (this.#validate(json)) {
                return [];
            }
        } catch (error) {
            throw new JsonSchemaValidatorError(`Failed to perform JSON validation: ${(error as Error).message}`, error as Error);
        }
        // Shouldn't happen...
        if (!this.#validate.errors?.length) {
            throw new JsonSchemaValidatorError('Internal Error: Validation failed but no errors were reported');
        }
        const numberOfErrors = this.#validate.errors.length;
        const result: JsonSchemaValidationFailure[] = [];
        for (let errorIndex = 0; errorIndex < numberOfErrors; errorIndex++) {
            const error: ErrorObject = this.#validate.errors[errorIndex];
            result.push({
                code: error.keyword,
                message: error.message,
                location: {
                    objectPath: error.instancePath,
                    schemaPath: error.schemaPath
                },
                context: error.params
            });
        }
        return result;
    }

    /**
     * Creates the compiled validator function we will use to validate JSON objects.
     * @param schema The schema to validate objects against.
     * @param options The options to be applied while validating JSON objects.
     * @returns An AJV validator function.
     * @throws [[InvalidJsonSchemaError]] If the supplied schema is invalid.
     */
    private static createValidator(schema: object, options?: JsonValidatorOptions): ValidateFunction<unknown> {
        const validatorFactory = new Ajv({
            allErrors: true, // return all errors, not just first error
            allowUnionTypes: true, // not technically legit, but makes for MUCH better error messages than allOf, anyOf, oneOf
            coerceTypes: false, // don't allow data type conversions
            discriminator: true, // allow discriminators
            strict: true, // this is the default, making explicit
            validateSchema: true // validates the input schema
        });
        try {
            // validate original schema
            validatorFactory.compile(schema);
            // update the schema as requested
            const updatedSchema = applyUnknownPropertiesBehavior(
                schema,
                options?.unknownPropertiesBehavior ?? UnknownPropertiesBehavior.AllowUnlessExplicitlyProhibited
            );
            // create the validator we'll use
            return validatorFactory.compile(updatedSchema);
        } catch (error) {
            if (error instanceof InvalidParameterError) {
                throw error;
            }
            throw new InvalidJsonSchemaError(`Schema is invalid: ${(error as Error).message}`, error as Error);
        }
    }
}
