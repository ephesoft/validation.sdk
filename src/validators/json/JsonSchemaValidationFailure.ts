/**
 * Represents a single JSON schema validation failure.
 */
export interface JsonSchemaValidationFailure {
    /**
     * Code representing the type of validation failure.
     */
    code: string;

    /**
     * A human-readable description of the failure.
     */
    message?: string;

    /**
     * Location data about the failure.
     */
    location: {
        /**
         * The path of the failed property.
         */
        objectPath: string;

        /**
         * The path in the JSON schema of the rule being validated.
         */
        schemaPath: string;
    };

    /**
     * Additional context information about the error.
     */
    context: Record<string, unknown>;
}
