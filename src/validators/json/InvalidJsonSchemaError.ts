import { NestedError } from '@ephesoft/core.sdk/errors/NestedError';

/**
 * Thrown when the schema passed to [[JsonSchemaValidator]] is found to be invalid.
 */
export class InvalidJsonSchemaError extends NestedError {
    /**
     * Initializes a new instance of the [[InvalidJsonSchemaError]] class.
     * @param message Description of the error.
     * @param innerError Error that caused the [[InvalidJsonSchemaError]].
     */
    public constructor(message: string, innerError?: Error) {
        super(message, innerError);
        this.name = 'InvalidJsonSchemaError';
    }
}
