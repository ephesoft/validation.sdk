/**
 * @name customWordsRegex
 * @param  {string[]} wordsArray
 * @returns RegExp
 * @example customWordsRegex(['home','test','new']).test('home')
 */
export const customWordsRegex = (wordsArray: string[]): RegExp => {
    let pattern = '';
    wordsArray.forEach((item, index): void => {
        pattern = pattern + item;
        if (wordsArray.length - 1 !== index) {
            pattern = pattern + '|';
        }
    });
    return new RegExp(pattern);
};
